<?php

namespace FullCycle\GoogleMarket;

use FullCycle\ApiFramework\ApiRequestor;
use Google_Client;
use FullCycle\GoogleMarket\GoogleMarketApiConfig;

class GoogleRequestor extends ApiRequestor {
	static $staticClient = false;

	function openClient() {
		if (static::$staticClient) {
			$this->client = static::$staticClient;
			return $this->client;
		}
		$google_config_file=GoogleMarketApiConfig::getApiFile();
		if (!$google_config_file) {
			throw new \Exception("Must set google market api config file");
		}
		putenv("GOOGLE_APPLICATION_CREDENTIALS=$google_config_file");
		$googleClient = new Google_Client();
		$googleClient->useApplicationDefaultCredentials();
		$googleClient->addScope("https://www.googleapis.com/auth/content");
		static::$staticClient = $this->client = $googleClient->authorize();
		return $this->client;
	}
}


