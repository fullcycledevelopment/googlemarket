<?php 

namespace FullCycle\GoogleMarket;

use FullCycle\GoogleMarket\GoogleMarketApiResource;
use FullCycle\ApiFramework\ApiConfigClass;

/**
 * @author thrasher
 * 
 * @example
 * 
 */

class GoogleMarketTestOrder extends GoogleMarketApiResource {
    protected $_request_url="testorders";
    protected $_method = "POST";
    
    function __construct($id = null, $opts = null) {
//	ApiConfigClass::setRequestor("\FullCycle\GoogleMarket\GoogleRequestor");
	parent::__construct($id,$opts);
    }
 
}
