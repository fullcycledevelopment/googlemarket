<?php 

namespace FullCycle\GoogleMarket;

use FullCycle\GoogleMarket\GoogleMarketApiResource;
use FullCycle\ApiFramework\Util\Util;

/**
 * @author thrasher
 * 
 * @example
 * 
 */

class GoogleMarketCancelLineItem extends GoogleMarketApiResource {
    protected $_request_url="orders";
    protected $_method = "POST";
    
    function __construct($id = null, $opts = null) {
	$uuid = Util::uuid4();
	parent::__construct($id,$opts);
	$this->_retrieveOptions = array_merge(['operationId' => $uuid],$this->_retrieveOptions);
    }

   function makeUri() {
	$uri=parent::makeUri();
	$uri = "$uri/{$this->id}/cancelLineItem";
	return $uri;
   }
 
}

