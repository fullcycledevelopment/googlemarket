<?php 

namespace FullCycle\GoogleMarket;

use FullCycle\GoogleMarket\GoogleMarketApiResource;

/**
 * @author thrasher
 * 
 * @example
 * 
 */

class GoogleMarketOrders extends GoogleMarketApiResource {
    protected $_request_url="orders";
    protected $_method = "GET";
    
    function __construct($id = null, $opts = null) {
	parent::__construct($id,$opts);
    }


    function next() {
		if (!$this->nextPageToken)
			return false;
		return GoogleMarketOrders::create(['pageToken' => $this->nextPageToken]);
    }
 
}

