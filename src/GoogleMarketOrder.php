<?php 

namespace FullCycle\GoogleMarket;

use FullCycle\GoogleMarket\GoogleMarketApiResource;
use FullCycle\ApiFramework\Util\Util;

/**
 * @author thrasher
 * 
 * @example
 * 
 */

class GoogleMarketOrder extends GoogleMarketApiResource {
    protected $_request_url="orders";
    protected $_method = "GET";
    
    function __construct($id = null, $opts = null) {
	parent::__construct($id,$opts);
    }

   function makeUri() {
	$uri=parent::makeUri();
	$uri = "$uri/{$this->id}";
	return $uri;
   }
 
}

