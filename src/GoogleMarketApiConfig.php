<?php

namespace FullCycle\GoogleMarket;

use FullCycle\ApiFramework\ApiConfigClass;
use \Exception;

class GoogleMarketApiConfig extends ApiConfigClass {
	static protected $apiBase="https://www.googleapis.com/content/v2sandbox";
	static protected $apiProductionBase = "https://www.googleapis.com/content/v2";
	static protected $apiSandboxBase = "https://www.googleapis.com/content/v2sandbox";
	static protected $apiVersion = "v2";
	static protected $merchantId = false;
	static protected $_apiFile = false;
	
	static function setProduction() {
	    static::$apiBase = static::$apiProductionBase;
	}
	
	static function setSandbox() {
	    static::$apiBase = static::$apiSandboxBase;
	}

	static function setMerchantId($id) {
		static::$merchantId = $id;
	}

	static function getMerchantId() {
		return static::$merchantId;
	}
	
	static function getVersion() {
	    return static::$apiVersion;
	}

	static function setApiFile($value) {
		static::$_apiFile = $value;
	}

	static function getApiFile() {
		return static::$_apiFile;
	}
}

