<?php

require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . "/env.php";

use FullCycle\GoogleMarket\GoogleMarketApiConfig;

$apiFile = env("GOOGLE_MARKET_API_FILE");
GoogleMarketApiConfig::setApiFile($apiFile);
GoogleMarketApiConfig::setMerchantId(env("GOOLGE_MARKET_MERCHANT_ID"));



