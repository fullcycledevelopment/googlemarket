<?php

require_once 'boot.php';

use FullCycle\GoogleMarket\GoogleMarketAcknowledgeOrder;
use FullCycle\GoogleMarket\GoogleMarketOrder;


if (empty($argv[1])) {
	echo "You must provide an order number\n";
	die();
}

$OrderId = $argv[1];

echo "Acknowledge order test\n\n";


$order = GoogleMarketOrder::create($OrderId);
echo "{$order->id} -- {$order->status} --  [{$order->acknowledged}]\n";

// Acknowledge Parameters
$params = [
	'id' => $OrderId,	
];

$ack = GoogleMarketAcknowledgeOrder::create($params);
$order = GoogleMarketOrder::create($OrderId);

echo "{$order->id} -- {$order->status} --  [{$order->acknowledged}]\n";



