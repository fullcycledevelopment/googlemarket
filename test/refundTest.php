<?php

require_once 'boot.php';

use FullCycle\GoogleMarket\GoogleMarketRefundOrder;
use FullCycle\GoogleMarket\GoogleMarketOrder;


if (empty($argv[1])) {
	echo "You must provide an order number\n";
	die();
}

#$cancelOrderId = "TEST-4071-31-7472";
$cancelOrderId = $argv[1];
$reason = "other";
$currency = "USD";

echo "Refund Test";


$order = GoogleMarketOrder::create($cancelOrderId);
print_r($order->toArray());

$itemAmounts = 0;
$taxAmount = 0;
$shippingAmount = $order->shippingCost->value;

foreach ($order->lineItems as $item) {
	$itemAmounts += $item->price->value;
	$taxAmount += $item->tax->value;
}

$amount = $order->netAmount->value;
$amountPretax = $itemAmounts + $shippingAmount;

echo "itemAmount: $itemAmounts\n";
echo "taxAmount: $taxAmount\n";

// Refund Parameters
$params = [
	'id' => $cancelOrderId,	
	'reason' => $reason,
	'amountPretax' => [
		'value' => $amountPretax,
		'currency' => $currency,
	],	

];

print_r($params);

$refund = GoogleMarketRefundOrder::create($params);

print_r($refund->toArray());
