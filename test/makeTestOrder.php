<?php


require_once 'boot.php';

use FullCycle\GoogleMarket\GoogleMarketOrders;
use FullCycle\GoogleMarket\GoogleMarketOrder;
use FullCycle\GoogleMarket\GoogleMarketTestOrder;
use FullCycle\GoogleMarket\GoogleMarketAdvanceOrder;
use FullCycle\GoogleMarket\GoogleMarketAcknowledgeOrder;
use FullCycle\GoogleMarket\GoogleMarketUpdateMerchantOrderId;
use FullCycle\GoogleMarket\GoogleMarketShipLineItems;

echo "Test Order Flow\n";

echo "Creating Test Order\n";

$params=[
	"country"=>"US",
  "testOrder"=>[
    "kind"=>"content#testOrder",
    "lineItems"=>[
      [
        "product"=> [
          "offerId"=>"15008-60002",
          "channel"=>"online",
          "targetCountry"=>"US",
          "contentLanguage"=>"en",
          "title"=>"My test product",
          "price"=>[
            "value"=>"12.00",
            "currency"=>"USD"
          ],
          "condition"=>"new",
          "brand"=>"mobstub",
          "mpn"=>"15008-60002",
        "itemGroupId"=>"123",
        "imageLink"=>" http://www.gstatic.com/shopping-content-api/product_images/nexus_9.jpg",
        ],
          
        "quantityOrdered"=>"2",
        "shippingDetails"=>[
        //  "type"=>"delivery",
          "method"=>[
            "methodName"=>"ShippingService#2",
            "carrier"=>"FedEx",
            "minDaysInTransit"=>3,
            "maxDaysInTransit"=>6
          ],
          "shipByDate"=>"2019-12-11T21:16:09Z",
          "deliverByDate"=>"2019-12-14T21:16:09Z"
        ],
        "returnInfo"=>[
          "isReturnable"=> "true",
          "daysToReturn"=>14,
          "policyUrl"=>"https://support.google.com/store/answer/2411741"
        ]
      ]
    ],
        
    "shippingOption"=>"standard",
    "customer"=>[
      "fullName"=>"Jim Halpert",
      "email"=>"pog.jim.halpert@gmail.com",
      "marketingRightsInfo"=>[
        "explicitMarketingPreference"=>"granted",
        "lastUpdatedTimestamp"=>"2019-12-14T21:16:09Z"
      ]
    ],
    "predefinedDeliveryAddress"=>"jim",
   // "predefinedPickupDetails"=>"jim",
    "paymentMethod"=>[
      "type"=>"VISA",
      "lastFourDigits"=>"1234",
      "predefinedBillingAddress"=>"jim",
      "expirationMonth"=>"12",
      "expirationYear"=>"2021"
    ],
    "shippingCost"=>[
      "value"=>"2",
      "currency"=>"USD"
    ],
    "shippingCostTax"=>[
      "value"=>"0",
      "currency"=>"USD"
    ],
    "notificationMode"=>"merchantPull",
    "enableOrderinvoices"=> "0"
      
  ]
];


//$testOrder = GoogleMarketTestOrder::create(['templateName' => 'template1' ]);
print_r($params);
$testOrder = GoogleMarketTestOrder::create($params);
print_r($testOrder->toArray());
$adv = GoogleMarketAdvanceOrder::create($testOrder->orderId);



