<?php

require_once 'boot.php';

use FullCycle\GoogleMarket\GoogleMarketCancelOrder;
use FullCycle\GoogleMarket\GoogleMarketOrder;


if (empty($argv[1])) {
	echo "You must provide an order number\n";
	die();
}

#$cancelOrderId = "TEST-4071-31-7472";
$cancelOrderId = $argv[1];
$reason = "other";
$currency = "USD";

echo "CancelTest";


$order = GoogleMarketOrder::create($cancelOrderId);
print_r($order->toArray());

$itemAmounts = 0;
$taxAmount = 0;
$shippingAmount = 0;

foreach ($order->lineItems as $item) {
	$itemAmounts += $item->price->value;
	$taxAmount += $item->tax->value;
}

$amount = $order->netAmount->value;
$amountPretax = $itemAmounts;

echo "itemAmount: $itemAmounts\n";
echo "taxAmount: $taxAmount\n";

// Refund Parameters
$params = [
	'id' => $cancelOrderId,	
	'amountPretax' => [
		'value' => $amountPretax,
		'currency' => $currency,
	],	

];

// Cancel Parameters
$params = [
	'id' => $cancelOrderId,	
	'reason' => $reason,
];

print_r($params);

$cancel = GoogleMarketCancelOrder::create($params);

print_r($cancel->toArray());
