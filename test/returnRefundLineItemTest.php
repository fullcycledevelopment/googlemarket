<?php

require_once 'boot.php';

use FullCycle\GoogleMarket\GoogleMarketReturnRefundLineItem;
use FullCycle\GoogleMarket\GoogleMarketOrder;


if (empty($argv[1])) {
	echo "You must provide an order number\n";
	die();
}

#$cancelOrderId = "TEST-4071-31-7472";
$cancelOrderId = $argv[1];
$reason = "other";
$currency = "USD";

echo "Return Refund Line Item Test";


$order = GoogleMarketOrder::create($cancelOrderId);
print_r($order->toArray());

$lineItem = null;
foreach($order->lineItems as $item) {
	if ($item->quantityOrdered - ($item->quantityCanceled + $item->quantityReturned) > 0) {
		$lineItem = $item;
		break;
	}	
}

if (!$lineItem) {
	echo "Nothing to cancel in order\n";
	die();
}
// $lineItem = $order->lineItems[0];
echo "Return Refund Line Item: \n";
print_r($lineItem->toArray());
// Line Item Parameters
$params = [
	'id' => $order->id,
	'productId' => $lineItem->product->id,
	'quantity' => $lineItem->quantityOrdered,
	'reason' => $reason,
	'amountPretax' => [
		'value' => $lineItem->price->value, // + $lineItem->shipping->amount,
		'currency' => "USD",
	],

];


echo "Return Refund Line Item Params\n";
print_r($params);

$cancel = GoogleMarketReturnRefundLineItem::create($params);

print_r($cancel->toArray());
