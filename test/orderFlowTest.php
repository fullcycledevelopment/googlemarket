<?php


require_once 'boot.php';

use FullCycle\GoogleMarket\GoogleMarketOrders;
use FullCycle\GoogleMarket\GoogleMarketOrder;
use FullCycle\GoogleMarket\GoogleMarketTestOrder;
use FullCycle\GoogleMarket\GoogleMarketAdvanceOrder;
use FullCycle\GoogleMarket\GoogleMarketAcknowledgeOrder;
use FullCycle\GoogleMarket\GoogleMarketUpdateMerchantOrderId;
use FullCycle\GoogleMarket\GoogleMarketShipLineItems;

echo "Test Order Flow\n";

echo "Creating Test Order\n";

$testOrder = GoogleMarketTestOrder::create(['templateName' => 'template1' ]);
print_r($testOrder->toArray());


$order = GoogleMarketOrder::create($testOrder->orderId);

echo "Fetched Order: \n";

print_r($order->toArray());

echo "{$order->id} -- {$order->status} --  [{$order->acknowledged}]\n";
echo "Advancing Order\n";
$adv = GoogleMarketAdvanceOrder::create($order->id);
print_r($adv->toArray());

$order = GoogleMarketOrder::create($testOrder->orderId);
echo "{$order->id} -- {$order->status} --  [{$order->acknowledged}]\n";

echo "Acknowledging Order\n";
$ack = GoogleMarketAcknowledgeOrder::create($order->id);
print_r($ack->toArray());

$order = GoogleMarketOrder::create($testOrder->orderId);
echo "{$order->id} -- {$order->status} --  [{$order->acknowledged}]\n";


shipOrder($order);

$order = GoogleMarketOrder::create($testOrder->orderId);
echo "{$order->id} -- {$order->status} --  [{$order->acknowledged}]\n";


function shipOrder($order) {
echo "Shipping Order\n";

        echo "Line Items\n";
        foreach ($order->lineItems as $item) {
                print_r($item->toArray());
                $shipParams = [
                        "id" => $order->id,
                        "lineItems" => [
                                [
                                "lineItemId" => $item->id,
                                "quantity" => $item->quantityOrdered,
                                ],
                        ],
			"shipmentInfos" => [[
	                        "shipmentId" => "shipment-{$item->id}",
				"carrier" => "FedEx",
				"trackingId" => "ASDFGHJKL12347890"
			]],

                ];
                print_r($shipParams);
                $shipResult = GoogleMarketShipLineItems::create($shipParams);
                print_r($shipResult->toArray());
        }


}
