<?php


require_once 'boot.php';

use FullCycle\GoogleMarket\GoogleMarketOrder;
use FullCycle\GoogleMarket\GoogleMarketTestOrder;
use FullCycle\GoogleMarket\GoogleMarketShipLineItems;

if (empty($argv[1])) {
        echo "You must provide an order number\n";
        die();
}

$cancelOrderId = $argv[1];


$order = GoogleMarketOrder::create($cancelOrderId);
print_r($order->toArray());

	echo "Line Items\n";
	foreach ($order->lineItems as $item) {
		print_r($item->toArray());
		$shipParams = [
			"id" => $order->id,
			  "shipmentId" => "shipment-{$item->id}",
  			"lineItems" => [ 
				[	
			      	"lineItemId" => $item->id,
			      	"quantity" => $item->quantityOrdered,
		    		],
  			],
  			"carrier" => "FedEx",
  			"trackingId" => "ASDFGHJKL12347890"

		];
		print_r($shipParams);
		$shipResult = GoogleMarketShipLineItems::create($shipParams);
		print_r($shipResult->toArray());
	}


